%undefine _hardened_build

Name:           gnat_util
Version:        8.2.0
Release:        1%{?dist}
Summary:        FIXME

License:        GPLv3+
URL:            http://gcc.gnu.org
## direct download is not available
Source0:        https://mirrors-usa.go-parts.com/gcc/releases/gcc-8.2.0/gcc-%{version}.tar.xz
Patch1:         %{name}-project.patch


BuildRequires:  gprbuild gcc-gnat fedora-gnat-project-common
##Requires:

%description
Gnat_Util is a library uniting a number of GNAT sources that are used by
different tools. At the moment Gnat_Util provides GNAT sources needed by ASIS,
GNATCOLL and Gprbuild. Its main purpose is allowing to simultaneously use
tools like ASIS and GNATCOLL (and other ones based on GNAT sources, if they
are to come), which would normally cause name conflicts between the same GNAT
sources that they use. Building both of them using same set of sources that
is Gnat_Util solves this problem.


%package devel
Summary:    Devel package for %{name}
License:    GPLv3+
Group:      Development/Libraries
Requires:   %{name}%{?_isa} = %{version}-%{release}

%description devel
%{summary}


%prep 
%setup -q -n gcc-%{version}
%patch1 -p1
make -C gcc -f ada/Make-generated.in ada/stamp-snames
sed "s/@FEDORA_LIBRARY_VERSION@/%{version}/" -i gcc/ada/gnatvsn.ads

%build
gprbuild -p -P gcc/ada/%{name}.gpr -XFEDORA_LIBRARY_VERSION=%{version} \
         %Gnatmake_optflags
%install
#rm -rf $RPM_BUILD_ROOT
gprinstall -p -P gcc/ada/%{name}.gpr -XFEDORA_LIBRARY_VERSION=%{version} \
           --prefix=%{_prefix} \
           --sources-subdir=%{buildroot}%{_prefix}/include/%{name} \
           --lib-subdir=%{buildroot}%{_libdir}/%{name} \
           --link-lib-subdir=%{buildroot}%{_libdir} \
           --project-subdir=%{buildroot}%{_GNAT_project_dir}
#%make_install prefix=%{buildroot}/%{_prefix} ENABLE_SHARED=yes BUILD_TYPE=Debug INSTALLER="gprinstall  -v"
## We don't need manifests
rm -rf %{buildroot}/%{_GNAT_project_dir}/manifests

#rm -rf %{buildroot}/%{_libdir}/%{name}/static*

%files
%license COPYING3
%dir %{_libdir}/%{name}
%{_libdir}/lib%{name}.so.%{version}
%{_libdir}/%{name}/lib%{name}.so.%{version}


%files devel
%{_libdir}/%{name}/*.ali
%{_includedir}/%{name}
%{_GNAT_project_dir}/%{name}.gpr
%{_libdir}/lib%{name}.so
%{_libdir}/%{name}/lib%{name}.so


%changelog
* Thu Oct 25 2018 Maxim Reznik <reznikmm@gmail.com>
- Use gcc sources to build gnat_util

* Wed Apr 19 2017 Pavel Zhukov <pavel@zhukoff.net>
- 
