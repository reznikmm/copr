## https://bugzilla.redhat.com/1197501
%undefine _hardened_build
%global build_tools 0

Name:           asis
Version:        2018
Release:        2%{?dist}
Summary:        Ada Semantic Interface Specification (ASIS) library

License:        GPLv3+
URL:            https://github.com/simonjwright/ASIS
Source0:        https://github.com/simonjwright/ASIS/archive/%{name}-gpl-%{version}-20180523-src-to-gcc8.tar.gz
Patch1:         %{name}-gpl-2018.patch

BuildRequires:  gprbuild
BuildRequires:  gcc-gnat
BuildRequires:  gnat_util-devel
BuildRequires:  fedora-gnat-project-common

# gprbuild only available on these:
ExclusiveArch: %GPRbuild_arches

%if %{build_tools}
## Keep all BRs as close as possible
BuildRequires:  xmlada-devel >= %{version}
BuildRequires:  gnatcoll-devel >= %{version}
%endif


%description
ASIS is a library that gives applications access to the complete syntactic
and semantic structure of an Ada compilation unit.
This library is typically used by tools that need to perform
some sort of static analysis on an Ada program.

%package devel
Summary:    Devel package for %{name}
License:    GPLv3+
Group:      Development/Libraries
Requires:   gnat_util-devel
Requires:   %{name}%{?_isa} = %{version}-%{release}

%description devel
%{summary}

%prep
%setup -q -n ASIS-%{name}-gpl-%{version}-20180523-src-to-gcc8
%patch1 -p1

%build
gprbuild -p -P %{name}.gpr -XFEDORA_LIBRARY_VERSION=%{version} \
         %Gnatmake_optflags

%if %{build_tools}
gprbuild -p -P %{name}_build.gpr -XFEDORA_LIBRARY_VERSION=%{version} \
         %Gnatmake_optflags
%endif

%install
gprinstall -p -P %{name}.gpr -XFEDORA_LIBRARY_VERSION=%{version} \
           --prefix=%{buildroot}%{_prefix} \
           --sources-subdir=%{buildroot}%{_prefix}/include/ \
           --lib-subdir=%{buildroot}%{_libdir} \
           --link-lib-subdir=%{buildroot}%{_libdir} \
           --project-subdir=%{buildroot}%{_GNAT_project_dir}


## We don't use manifest nor plan to use them
rm -rf %{buildroot}/%{_GNAT_project_dir}/manifests

## GPS is not packaged (yet?)
rm -rf %{buildroot}/%{_datadir}/gps

## Drop documentation for now
rm -rf %{buildroot}/%{_prefix}/share/doc/asis/

%files
%license COPYING3
%doc README 
%dir %{_libdir}/%{name}
%{_libdir}/lib%{name}.so.%{version}
%{_libdir}/%{name}/lib%{name}.so.%{version}

%files devel
%{_libdir}/%{name}/*.ali
%{_includedir}/%{name}
%{_GNAT_project_dir}/%{name}.gpr
%{_libdir}/lib%{name}.so
%{_libdir}/%{name}/lib%{name}.so


%changelog
* Sun Nov 04 2018 Maxim Reznik <reznikmm@gmail.com> - 2018-2
- Add gnat_util-devel as requirements of devel package

* Sun Nov 04 2018 Maxim Reznik <reznikmm@gmail.com> - 2018-1
- Update to asis gpl 2018

* Wed Apr 19 2017 Pavel Zhukov <pavel@zhukoff.net> - 2016.1
- Initial build
